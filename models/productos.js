'use strict'

var mongoose = require('mongoose');
var squema = mongoose.Schema;

var productoSchema = squema({
  nombre: String,
  descripcion: String,
  precio: String,
  imagen: String
});

module.exports = mongoose.model('Productos', productoSchema);
